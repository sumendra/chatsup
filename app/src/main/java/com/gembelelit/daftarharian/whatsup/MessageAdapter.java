package com.gembelelit.daftarharian.whatsup;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {
    List<Messages> userMessage;
    FirebaseAuth mAuth;
    DatabaseReference userRef;
    public MessageAdapter(List<Messages> userMessage){
        this.userMessage=userMessage;
    }

    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_message_layout, viewGroup, false);
        mAuth=FirebaseAuth.getInstance();
        return new MessageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageHolder messageHolder, final int i) {
        // user yang login sekarang
        String messageSender=mAuth.getCurrentUser().getUid();
        Messages messages = userMessage.get(i);
        // menangkap id user yang dikirimmi pesan
        String fromUserId = messages.getFrom();
        String fromMessageType = messages.getType();
        // mengambil image nama dari user berdasarkan fromuserid
        userRef= FirebaseDatabase.getInstance().getReference().child("Users").child(fromUserId);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("image")){
                    String receiverimage = dataSnapshot.child("image").getValue().toString();
                    Picasso.get().load(receiverimage).placeholder(R.drawable.image_profile).into(messageHolder.profilImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        // mengeset semua gone di awal
        messageHolder.receiverMessage.setVisibility(View.GONE);
        messageHolder.profilImage.setVisibility(View.GONE);
        messageHolder.senderMessage.setVisibility(View.GONE);
        messageHolder.messageSenderPicture.setVisibility(View.GONE);
        messageHolder.messageReiceverPicture.setVisibility(View.GONE);
        if(fromMessageType.equals("text")){
            // ketika message id sama dengan id user berarti user yang sedang login yang mengirim message
            if(fromUserId.equals(messageSender)){
                messageHolder.senderMessage.setVisibility(View.VISIBLE);
                messageHolder.senderMessage.setBackgroundResource(R.drawable.sender_message_layout);
                messageHolder.senderMessage.setText(messages.getMessage()+"\n \n "+messages.getTime()+" - "+messages.getDate());
            }
            // ketika status user login sebagai penerima pesan
            else{
//                messageHolder.receiverMessage.setVisibility(View.VISIBLE);
//                messageHolder.senderMessage.setBackgroundResource(R.drawable.receiver_message_layout);
                messageHolder.receiverMessage.setVisibility(View.VISIBLE);
                messageHolder.profilImage.setVisibility(View.VISIBLE);
                messageHolder.receiverMessage.setBackgroundResource(R.drawable.receiver_message_layout);
                messageHolder.receiverMessage.setText(messages.getMessage()+"\n \n "+messages.getTime()+" - "+messages.getDate());
            }
        }
        else if(fromMessageType.equals("image")){
            if(fromUserId.equals(messageSender)){
                messageHolder.messageSenderPicture.setVisibility(View.VISIBLE);
                Picasso.get().load(messages.getMessage()).into(messageHolder.messageSenderPicture);
            }
            else{
                messageHolder.profilImage.setVisibility(View.VISIBLE);
                messageHolder.messageReiceverPicture.setVisibility(View.VISIBLE);
                Picasso.get().load(messages.getMessage()).into(messageHolder.messageReiceverPicture);
            }
        }
        else if(fromMessageType.equals("pdf") || fromMessageType.equals("docx")){
            if (fromUserId.equals(messageSender)) {
                messageHolder.messageSenderPicture.setVisibility(View.VISIBLE);
                messageHolder.messageSenderPicture.setBackgroundResource(R.drawable.file);
                // jika mengklik file maka akan mendownload file
//                messageHolder.itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {

//                    }
//                });
            }
            else{
                messageHolder.profilImage.setVisibility(View.VISIBLE);
                messageHolder.messageReiceverPicture.setVisibility(View.VISIBLE);
                messageHolder.messageReiceverPicture.setBackgroundResource(R.drawable.file);
                // if click file maka akan mendownload file
//                messageHolder.itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(userMessage.get(i).getMessage()));
//                        messageHolder.itemView.getContext().startActivity(intent);
//                    }
//                });
            }
        }
        if(fromUserId.equals(messageSender)){
            // ketika recycler view di klik
            messageHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(userMessage.get(i).getType().equals("image")){
                        CharSequence op [] = new CharSequence[]{
                                "Delete For me",
                                "View This Image",
                                "Cancel",
                                "Delete For Everyone"
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(messageHolder.itemView.getContext());
                        builder.setTitle("Delete Message ?");
                        builder.setItems(op, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which==0){
                                    deleteSendMessage(i, messageHolder);
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),MainActivity.class);
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which == 1){
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),ImageViewActivity.class);
                                    // mengirim message ke class activity image view
                                    intent.putExtra("url", userMessage.get(i).getMessage());
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which == 2){
                                    dialog.dismiss();
                                }
                                else if(which==3){
                                    deleteForEverMessage(i, messageHolder);
                                }
                            }
                        });
                        builder.show();
                    }
                    else if(userMessage.get(i).getType().equals("text")){
                        CharSequence op [] = new CharSequence[]{
                                "Delete For me",
                                "Cancel",
                                "Delete For Everyone"
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(messageHolder.itemView.getContext());
                        builder.setTitle("Delete Message ?");
                        builder.setItems(op, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which==0){
                                    deleteSendMessage(i, messageHolder);
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),MainActivity.class);
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which == 1){
                                   dialog.dismiss();
                                }
                                else if(which==2){
                                    deleteForEverMessage(i, messageHolder);
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),MainActivity.class);
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                            }
                        });
                        builder.show();
                    }
                    else if(userMessage.get(i).getType().equals("pdf") || userMessage.get(i).getType().equals("docx")){
                        CharSequence op [] = new CharSequence[]{
                                "Delete For me",
                                "Download and View This Document",
                                "Cancel",
                                "Delete For Everyone"
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(messageHolder.itemView.getContext());
                        builder.setTitle("Delete Message ?");
                        builder.setItems(op, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which==0){
                                    deleteSendMessage(i, messageHolder);
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),MainActivity.class);
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which == 1){
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(userMessage.get(i).getMessage()));
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which == 2){
                                    dialog.dismiss();
                                }
                                else if(which==3){
                                    deleteForEverMessage(i, messageHolder);
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),MainActivity.class);
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                            }
                        });
                        builder.show();
                    }
                }
            });
        }
        else{
            // ketika recycler view di klik
            messageHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(userMessage.get(i).getType().equals("image")){
                        CharSequence op [] = new CharSequence[]{
                                "Delete For me",
                                "View This Image",
                                "Cancel"
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(messageHolder.itemView.getContext());
                        builder.setTitle("Delete Message ?");
                        builder.setItems(op, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which==0){
                                    deleteReceiveMessage(i, messageHolder);
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),MainActivity.class);
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which == 1){
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),ImageViewActivity.class);
                                    // mengirim message ke class activity image view
                                    intent.putExtra("url", userMessage.get(i).getMessage());
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which == 2){
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    }
                    else if(userMessage.get(i).getType().equals("text")){
                        CharSequence op [] = new CharSequence[]{
                                "Delete For me",
                                "Cancel"
                        };
                        final AlertDialog.Builder builder = new AlertDialog.Builder(messageHolder.itemView.getContext());
                        builder.setTitle("Delete Message ?");
                        builder.setItems(op, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which==0){
                                    deleteReceiveMessage(i, messageHolder);
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),MainActivity.class);
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which==1){
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    }
                    else if(userMessage.get(i).getType().equals("pdf") || userMessage.get(i).getType().equals("docx")){
                        CharSequence op [] = new CharSequence[]{
                                "Delete For me",
                                "Download and View This Document",
                                "Cancel"
                        };
                        AlertDialog.Builder builder = new AlertDialog.Builder(messageHolder.itemView.getContext());
                        builder.setTitle("Delete Message ?");
                        builder.setItems(op, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(which==0){
                                    deleteReceiveMessage(i, messageHolder);
                                    Intent intent = new Intent(messageHolder.itemView.getContext(),MainActivity.class);
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                                else if(which == 2){
                                    dialog.dismiss();
                                }
                                else if(which == 1){
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(userMessage.get(i).getMessage()));
                                    messageHolder.itemView.getContext().startActivity(intent);
                                }
                            }
                        });
                        builder.show();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return userMessage.size();
    }

    public class MessageHolder extends RecyclerView.ViewHolder{
        TextView senderMessage, receiverMessage;
        CircleImageView profilImage;
        ImageView messageSenderPicture, messageReiceverPicture;
        public MessageHolder(@NonNull View itemView) {
            super(itemView);
            senderMessage=itemView.findViewById(R.id.tvSendMessageLayout);
            receiverMessage=itemView.findViewById(R.id.tvReceiverMessageLayout);
            profilImage=itemView.findViewById(R.id.imageMessage);
            messageReiceverPicture = itemView.findViewById(R.id.ivReceiverImage);
            messageSenderPicture= itemView.findViewById(R.id.ivSendImage);
        }
    }
    private  void deleteSendMessage(final int position, final MessageHolder messageHolder){
        DatabaseReference RootRef =FirebaseDatabase.getInstance().getReference();
        RootRef.child("message").child(userMessage.get(position).getFrom()).child(userMessage.get(position).getTo()).child(userMessage.get(position).getMessageId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(messageHolder.itemView.getContext(), "Delete Success" , Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(messageHolder.itemView.getContext(), "Error delete" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private  void deleteReceiveMessage(final int position, final MessageHolder messageHolder){
        DatabaseReference RootRef =FirebaseDatabase.getInstance().getReference();
        RootRef.child("message").child(userMessage.get(position).getTo()).child(userMessage.get(position).getFrom()).child(userMessage.get(position).getMessageId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(messageHolder.itemView.getContext(), "Delete Success" , Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(messageHolder.itemView.getContext(), "Error delete" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private  void deleteForEverMessage(final int position, final MessageHolder messageHolder){
        final DatabaseReference RootRef =FirebaseDatabase.getInstance().getReference();
        RootRef.child("message").child(userMessage.get(position).getTo()).child(userMessage.get(position).getFrom()).child(userMessage.get(position).getMessageId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    RootRef.child("message").child(userMessage.get(position).getFrom()).child(userMessage.get(position).getTo()).child(userMessage.get(position).getMessageId()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(messageHolder.itemView.getContext(), "Delete success" , Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }
                else{
                    Toast.makeText(messageHolder.itemView.getContext(), "Error delete" , Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
