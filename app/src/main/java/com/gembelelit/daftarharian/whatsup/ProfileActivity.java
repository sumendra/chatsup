package com.gembelelit.daftarharian.whatsup;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private String receiverUserId;
    private CircleImageView ivProfil;
    private TextView nameProfil, statusProfil;
    private Button buttonSendMessageProfil;
    private DatabaseReference UserRef, ChatRequestRef;
    private String currentState;
    private String currentUserId;
    private FirebaseAuth mAuth;
    private Button buttonCancelMessageProfil;
    private DatabaseReference ContactRef;
    DatabaseReference notificationRef;









    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initial id
        setContentView(R.layout.activity_profile);
        ivProfil=findViewById(R.id.iv_profil);
        nameProfil=findViewById(R.id.nameProfil);
        statusProfil=findViewById(R.id.statusProfil);
        buttonSendMessageProfil=findViewById(R.id.button_send_message_profile);
        buttonCancelMessageProfil=findViewById(R.id.button_cancel_message_profile);
        // call currentstate
        currentState = "new";
        // initial fiirebasedatabase
        UserRef= FirebaseDatabase.getInstance().getReference().child("Users");
        mAuth= FirebaseAuth.getInstance();
        currentUserId=mAuth.getCurrentUser().getUid();
        ContactRef = FirebaseDatabase.getInstance().getReference().child("Contacts");
        // make file json name of Chat Request
        ChatRequestRef= FirebaseDatabase.getInstance().getReference().child("Chat Requests");
        // mengambil profile user yang di kirim melalui findfriend activity
        receiverUserId = getIntent().getExtras().get("visit_user_id").toString();
        // inialisasi databse
        notificationRef = FirebaseDatabase.getInstance().getReference().child("Notifications");

        retrieveUserInfoProfil();
    }

    // menyelect name status user ketika  profileactivity
    private void retrieveUserInfoProfil() {
        UserRef.child(receiverUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // jika user sudah mengubah image
                if((dataSnapshot.exists()) && (dataSnapshot.hasChild("image"))){
                    String userImage = dataSnapshot.child("image").getValue().toString();
                    String userName = dataSnapshot.child("name").getValue().toString();
                    String userStatus = dataSnapshot.child("status").getValue().toString();
                    Picasso.get().load(userImage).placeholder(R.drawable.image_profile).into(ivProfil);
                    nameProfil.setText(userName);
                    statusProfil.setText(userStatus);
                    ManageChatRequestProfil();
                }
                // jika user belum mengubah image
                else{
                    String userName = dataSnapshot.child("name").getValue().toString();
                    String userStatus = dataSnapshot.child("status").getValue().toString();
                    nameProfil.setText(userName);
                    statusProfil.setText(userStatus);
                    ManageChatRequestProfil();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void ManageChatRequestProfil() {
        ChatRequestRef.child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // jika child dari id penerima
                if(dataSnapshot.hasChild(receiverUserId)){
                    // mengambil request_type dari id_penerima
                    String request_type = dataSnapshot.child(receiverUserId).child("request_type").getValue().toString();
                    // ketika current id child  id_penerima requet_type memiliki value send
                    if(request_type.equals("send")){
                        // membuat status currentstate menjadi request_send
                        currentState = "request_send";
                        // maka button akan di edit menjadi cancel chat request
                        buttonSendMessageProfil.setText("Cancel Chat Request");
                    }
                    // ketika current id child id_penerima memiliki value received
                    else if(request_type.equals("receiver")){
                        currentState ="request_receiver";
                        // maka button Accept Chat Request
                        buttonSendMessageProfil.setText("Accept Chat Request");
                        // menampilkan button cancel request
                        buttonCancelMessageProfil.setVisibility(View.VISIBLE);
                        buttonCancelMessageProfil.setEnabled(true);
                        buttonCancelMessageProfil.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CalcelChatRequestProfil();
                            }
                        });
                    }
                }
                else{
                    // memberi status jika data contactref child currentid memiliki child receiverid maka teman
                    ContactRef.child(currentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.hasChild(receiverUserId)){
                                currentState="friends";
                                buttonSendMessageProfil.setText("Remove this Contact");
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        if(currentUserId.equals(receiverUserId)){
            // akan menghilangkan button jika id penerima sama dengan id pengirim
            buttonSendMessageProfil.setVisibility(View.INVISIBLE);
        }
        else{
            buttonSendMessageProfil.setVisibility(View.VISIBLE);
            buttonSendMessageProfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // membuat button tidak aktif saat di klik
                    buttonSendMessageProfil.setEnabled(false);
                    // artinya jika belum pernah berteman  maka akan  sendchatrequestprofile
                    if(currentState.equals("new")){
                        SendChatRequestProfil();
                    }
                    // jika request friend tidak baru
                    if(currentState.equals("request_send")){
                        CalcelChatRequestProfil();
                    }
                    if(currentState.equals("request_receiver")){
                        AcceptChatRequet();
                    }
                    // ketika bvtton send diklik dan state adalah friend
                    if(currentState.equals("friends")){
                        RemoveFriends();
                    }

                }
            });
        }
    }

    // while remove friends from contact
    private void RemoveFriends() {
        // remove value
        ContactRef.child(currentUserId).child(receiverUserId).
                removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    // remove value
                    ContactRef.child(receiverUserId).child(currentUserId).
                            removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                buttonSendMessageProfil.setEnabled(true);
                                // make status to new
                                currentState = "new";
                                buttonSendMessageProfil.setText("Send Message");
                                buttonCancelMessageProfil.setEnabled(false);
                                buttonCancelMessageProfil.setVisibility(View.INVISIBLE);
                            }
                        }
                    });
                }
            }
        });
    }

    private void AcceptChatRequet() {
        // membuat value Contact save
        ContactRef.child(currentUserId).child(receiverUserId).child("Contacts").setValue("saved").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    // membuat value Contact save
                    ContactRef.child(receiverUserId).child(currentUserId).child("Contacts").setValue("saved").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                // menghapus request
                                ChatRequestRef.child(currentUserId).child(receiverUserId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            // menghapus request
                                            ChatRequestRef.child(receiverUserId).child(currentUserId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()){
                                                        // membuat button send message status true
                                                        buttonSendMessageProfil.setEnabled(true);
                                                        // status state menjadi temnan
                                                        currentState ="friends";
                                                        // mengubah text button menjadi remove this contact
                                                        buttonSendMessageProfil.setText("Remove this Contact");
                                                        // menyembunyikan button
                                                        buttonCancelMessageProfil.setVisibility(View.INVISIBLE);
                                                        buttonCancelMessageProfil.setEnabled(false);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }

    // jika status sudah berteman maka bisa melakukan cancel pertemanan dengan meremoveValue yaitu request_type
    private void CalcelChatRequestProfil() {
        ChatRequestRef.child(currentUserId).child(receiverUserId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    ChatRequestRef.child(receiverUserId).child(currentUserId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                buttonSendMessageProfil.setEnabled(true);
                                // mengembalikan status menjadi baru
                                currentState = "new";
                                // mengembalikan button ke semula
                                buttonSendMessageProfil.setText("Send Message");
                                // membuat visibility cancel hilang agar pada ssat status penerima button cancel tidak terdapat
                                buttonCancelMessageProfil.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            }
        });
    }

    // receiver current id adalah id dari user yang akan kita send message
    // currentuserid adalah id dari user yang sedang login atau sedang akan mengesend message
    private void SendChatRequestProfil() {
        // menyimpan dalam id pengirim lalu id penerima lalu request_type dengan value send
        ChatRequestRef.child(currentUserId).child(receiverUserId).child("request_type").setValue("send").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                // jika mengirim pesan  success
                if(task.isSuccessful()){
                    // menyimpan  di dalam ChatRequestRef lalu di dalam id penerima lalu di dalam id pengirim lalu di dalam request_type
                    ChatRequestRef.child(receiverUserId).child(currentUserId).child("request_type").setValue("receiver").addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                // setnotification if have request contact
                                HashMap<String, String> chatNotification = new HashMap<>();
                                // take id user yang login sekarang
                                chatNotification.put("from", currentUserId);
                                chatNotification.put("type", "request");
                                // membuat database baru notification
                                notificationRef.child(receiverUserId).push()
                                .setValue(chatNotification).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            buttonSendMessageProfil.setEnabled(true);
                                            currentState = "request_send";
                                            buttonSendMessageProfil.setText("Cancel Chat Request");
                                        }
                                    }
                                });
                                // send message dapat diklik
                                // membuat aktif button saat di klik
                                buttonSendMessageProfil.setEnabled(true);
                                // jika sudah pernah berteman  melakukan request maka currentstate akan status request_send
                                currentState = "request_send";
                                // mengubah button text menjadi Cancel Chat Request
                                buttonSendMessageProfil.setText("Cancel Chat Request");
                            }
                        }
                    });
                }
            }
        });
    }
}
// request_send meminta pertemanan
// request_receiver diminta pertemanan
// new baru
// friends teman
