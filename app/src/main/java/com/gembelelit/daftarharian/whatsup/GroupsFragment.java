package com.gembelelit.daftarharian.whatsup;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsFragment extends Fragment {

    private  View groupView;
    private ListView list_view;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> list_of_groups = new ArrayList<>();
    private DatabaseReference GroupPreference;


    public GroupsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        groupView = inflater.inflate(R.layout.fragment_groups, container, false);
        list_view = (ListView) groupView.findViewById(R.id.main_group);
        arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, list_of_groups);
        list_view.setAdapter(arrayAdapter);

//        initial grouppreference
        GroupPreference = FirebaseDatabase.getInstance().getReference();
        RetrieveDisplay();


        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                get id listview
                String currentGroupName = parent.getItemAtPosition(position).toString();
                Intent groupChatIntent = new Intent(getContext(), GroupChatActivity.class);
//                intent to groupChatActivity
                groupChatIntent.putExtra("groupName", currentGroupName);
                startActivity(groupChatIntent);
            }
        });

        return groupView;
    }

    private void RetrieveDisplay() {
        GroupPreference.child("Groups").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // get user
                Iterator iterator  = dataSnapshot.getChildren().iterator();
                // menampung nama group user
                Set<String> set = new HashSet<>();
                // melakukan preulangan selama interator bisA di next
                while(iterator.hasNext()){
                    // menambahkan nama group user ke dalam hasset
                    set.add(((DataSnapshot)iterator.next()).getKey());
                }
                list_of_groups.clear();
                // menambahkan hasset ke dalam arraylist untuk adapter
                list_of_groups.addAll(set);
                // mwrefresh adapter
                arrayAdapter.notifyDataSetChanged();;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
