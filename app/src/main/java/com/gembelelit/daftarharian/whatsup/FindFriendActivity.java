package com.gembelelit.daftarharian.whatsup;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FindFriendActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private RecyclerView FindFriendRecyclerList;


    private DatabaseReference UserRef;










    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_friend);
        mToolbar=findViewById(R.id.find_friends_toolbar);
        FindFriendRecyclerList = findViewById(R.id.find_frineds_recycler_list);
        //set layoutmanager
        FindFriendRecyclerList.setLayoutManager(new LinearLayoutManager(this));
        //set title
        setSupportActionBar(mToolbar);
        // memberi tombol kembali main
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        // memberi title
        getSupportActionBar().setTitle("Find Friends");
        // initialisais firebase
        UserRef = FirebaseDatabase.getInstance().getReference().child("Users");
    }

    @Override
    protected void onStart() {
        super.onStart();
        // contex is value about name status
        FirebaseRecyclerOptions<Context> options = new FirebaseRecyclerOptions.Builder<Context>()
                .setQuery(UserRef, Context.class)
                .build();
        // add adapter
        FirebaseRecyclerAdapter<Context,FindFrienViewHolder> adapter = new FirebaseRecyclerAdapter<Context, FindFrienViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull FindFrienViewHolder holder, final int position, @NonNull Context model) {
                holder.tvName.setText(model.getName());
                holder.tvStatus.setText(model.getStatus());
                Picasso.get().load(model.getImage()).placeholder(R.drawable.image_profile).into(holder.imagePengguna);
                // take onClick in here
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // to take position recycler next to getkey user
                        String visit_user_id = getRef(position).getKey();
                        Intent intent = new Intent(FindFriendActivity.this, ProfileActivity.class);
                        // mengirim id dari pengguna berdasarkan position
                        intent.putExtra("visit_user_id", visit_user_id);
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public FindFrienViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_display_layout, viewGroup,false);
                FindFrienViewHolder viewHolder = new FindFrienViewHolder(view);
                return  viewHolder;
            }
        };
        FindFriendRecyclerList.setAdapter(adapter);
        adapter.startListening();
    }
    public static class FindFrienViewHolder extends RecyclerView.ViewHolder{
        TextView tvName, tvStatus;
        CircleImageView imagePengguna;

        public FindFrienViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tvName);
            tvStatus=itemView.findViewById(R.id.tvStatus);
            imagePengguna=itemView.findViewById(R.id.gambarpengguna);
        }
    }
}
