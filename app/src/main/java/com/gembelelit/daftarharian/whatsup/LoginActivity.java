package com.gembelelit.daftarharian.whatsup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class LoginActivity extends AppCompatActivity {

//    mengecek apakah terdapat user atau tidak
//    FirebaseUser currentUser;
    Button loginButton, phoneLogin;
    EditText etEmailMasuk, etPasswordMasuk;
    TextView tvBuatAkunBaru, tvLupaPassword;
    FirebaseAuth mAuth;
    ProgressDialog loadingbar;
    DatabaseReference UserRef;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // inisial activity
        loginButton=findViewById(R.id.login_button);
        phoneLogin=findViewById(R.id.login_dengan_phone_loginactivity);
        etEmailMasuk=findViewById(R.id.login_email);
        etPasswordMasuk=findViewById(R.id.login_password);
        tvBuatAkunBaru=findViewById(R.id.tv_membutuhkan_akun_loginactivity);
        tvLupaPassword=findViewById(R.id.tv_forgot_password_loginactivity);

        // take auth in firebase
        mAuth=FirebaseAuth.getInstance();
        UserRef = FirebaseDatabase.getInstance().getReference().child("Users");
//        currentUser = mAuth.getCurrentUser();
//        initialisasi loadingbar
        loadingbar = new ProgressDialog(this);
        // make onclick
        tvBuatAkunBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendUserToRegisterActivity();
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllowUserToLogin();
            }
        });
        // start to phone login
        phoneLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, PhoneLoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void AllowUserToLogin() {
        String email = etEmailMasuk.getText().toString();
        String password = etPasswordMasuk.getText().toString();
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this, "required email", Toast.LENGTH_SHORT).show();
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(this, "required password", Toast.LENGTH_SHORT).show();
        }else{
//            loading to progress masuk
            loadingbar.setTitle("Sign in");
            loadingbar.setMessage("Please wait, while sign in");
            loadingbar.setCanceledOnTouchOutside(true);
            loadingbar.show();
//            make signin with email password
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                String currentUserId = mAuth.getCurrentUser().getUid();
                                String devicetoken = FirebaseInstanceId.getInstance().getToken();
                                UserRef.child(currentUserId).child("device_token").setValue(devicetoken).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Toast.makeText(LoginActivity.this, "token save", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                Toast.makeText(LoginActivity.this, "succes login", Toast.LENGTH_SHORT).show();
                                SendUserToMainActivity();
//                                if progrees alreay dismis loading
                                loadingbar.dismiss();
                            }
                            else{
                                String message = task.getException().toString();
                                Toast.makeText(LoginActivity.this, message+"Error", Toast.LENGTH_SHORT).show();
                                loadingbar.dismiss();
                            }
                        }
                    });
        }
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
////        ketika currentuser tidak null maka send ke main activity
//        if (currentUser != null) {
//            SendUserToMainActivity();
//        }
//    }
    public void SendUserToMainActivity(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }
    public void SendUserToRegisterActivity(){
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
