package com.gembelelit.daftarharian.whatsup;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

public class GroupChatActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton SendMessageButton;
    private EditText userMessage;
    private ScrollView scrollView;;
    private TextView displayText;

    private String currentGroupName;
    private FirebaseAuth mAu;
    private String currentUserId, currentUserName;
    private DatabaseReference userRef, GroupnameRef;
    private String currentTime;
    private String currentDate;
    private DatabaseReference groupMessageKeyRef;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat);
//        inityal id from activity_group_chat
        toolbar=findViewById(R.id.group_chat);
        SendMessageButton=findViewById(R.id.send_message);
        userMessage=findViewById(R.id.tv_group_message);
        scrollView=findViewById(R.id.my_scroll);
        displayText=findViewById(R.id.tvgroup_chat);
        //get bundle yang di kirim dari groupsfragment
        currentGroupName = getIntent().getExtras().get("groupName").toString();
//        inityal toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(currentGroupName);
        // initial auth firebase
        mAu=FirebaseAuth.getInstance();
        currentUserId=mAu.getCurrentUser().getUid();
        userRef = FirebaseDatabase.getInstance().getReference().child("Users");
        GroupnameRef= FirebaseDatabase.getInstance().getReference().child("Groups").child(currentGroupName);

        // mengambil informasi user yang chat
        UserInfo();
        SendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveMessageInfoToDatabase();
                // setiap selesai melakukan save info maka settext ""
                userMessage.setText("");
//                make auto scroll if send message
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        // mengambil name chat to display
        GroupnameRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if(dataSnapshot.exists()){
                    // display name
                    DisplayMessage(dataSnapshot);

                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                DisplayMessage(dataSnapshot);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //append membuat displaytext terus membuat text baru dengan tulisan seusai dengan yang di append berbeda dengan setText yang lebih mengubah tulisan text
    // mengambil nama chat yang terdapat dalam group sesuai dengan namagroup
    private void DisplayMessage(DataSnapshot dataSnapshot) {
        // iterator digunakan menampung jumlah message lalu menampilkan melalui perulangan dimana ketika iterator.hasnext maka akan menampilkan message
        Iterator iterator = dataSnapshot.getChildren().iterator();
        //ketika itrator masih terus berjalan
        while(iterator.hasNext()){
            // menyimpan name time chat date dalam variable secara berurtan seperti file json di database
            String chatDate = (String) ((DataSnapshot)iterator.next()).getValue();
            String chatMessage = (String) ((DataSnapshot)iterator.next()).getValue();
            String chatName = (String) ((DataSnapshot)iterator.next()).getValue();
            String chatTime = (String) ((DataSnapshot)iterator.next()).getValue();

            //menampilkan hasil penyimpakan kedalam text
            displayText.append(chatName+":\n"+chatMessage+"\n"+chatTime +"    "+chatDate+"\n\n\n");

            scrollView.fullScroll(ScrollView.FOCUS_DOWN);

        }
    }

    // mengambil info dari pengguna
    private void UserInfo() {
        // mengambil berdasarkan id dari curentUserId
        userRef.child(currentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // mengambil nama
                currentUserName=dataSnapshot.child("name").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void SaveMessageInfoToDatabase() {
        String message = userMessage.getText().toString();
        // megambil key dari setiap pengguna chat
        String messageKey = GroupnameRef.push().getKey();
        if(TextUtils.isEmpty(message)){
            // ketika klik send namun editext kosong maka akan memberi toast
            Toast.makeText(this,  "Please write message first..." , Toast.LENGTH_SHORT).show();
        }else{
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM dd, yyyy ");
            currentDate = simpleDateFormat.format(calendar.getTime());

            Calendar calendar1 = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("hh:mm a ");
            currentTime = simpleDateFormat1.format(calendar1.getTime());

            // akan melakukan insert sesuai dengan key dari message
            // key setiap message akan berubah ubah sesuai dengan ketentuan firebase
            HashMap<String, Object> groupMessageKey = new HashMap<>();
            GroupnameRef.updateChildren(groupMessageKey);
            // mengambil key chat lalu menampung ke dalam database firebase
            groupMessageKeyRef = GroupnameRef.child(messageKey);

            HashMap<String, Object> messageInfoMap = new HashMap<>();
            messageInfoMap.put("name", currentUserName);
            messageInfoMap.put("message", message);
            messageInfoMap.put("date", currentDate);
            messageInfoMap.put("time", currentTime);
            // menambahkan child berserta value dalam database firebase sesuai dengan key yang telah di tambahkan sebelumnya
            groupMessageKeyRef.updateChildren(messageInfoMap);


        }
    }



}
// push().getkey digunakan untuk mengambil key dari setiap bagian file json baik itu users groups daln lainnya
// syarat di harus berupa child bukan value