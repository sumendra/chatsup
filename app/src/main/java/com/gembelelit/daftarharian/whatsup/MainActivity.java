package com.gembelelit.daftarharian.whatsup;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    Toolbar mToolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    TabAccesorAdapter tabAccesorAdapter;

    FirebaseAuth mAuth;
    private DatabaseReference RootRef;
    String currentUserId;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.main_page_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("WhatsApp");
//        initilil firebase
        mAuth= FirebaseAuth.getInstance();
        //implement viewpager
        viewPager = (ViewPager) findViewById(R.id.main_tabs_pager);
        tabAccesorAdapter = new TabAccesorAdapter(getSupportFragmentManager());
        viewPager.setAdapter(tabAccesorAdapter);
        tabLayout = findViewById(R.id.main_tabs);
        tabLayout.setupWithViewPager(viewPager);
//        initialisasi database
        RootRef= FirebaseDatabase.getInstance().getReference();

    }

    @Override
    protected void onStart() {
        FirebaseUser currentUser=  mAuth.getCurrentUser();
        super.onStart();
//        ketika currentuser firebase kosong
        if(currentUser == null){
            SendUserToLoginActivity();
        }
        else{
            updateUserStatus("online");
            VerifyUserExistance();
        }

    }

    @Override
    protected void onStop() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        super.onStop();
        if(currentUser != null){
            updateUserStatus("offline");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            updateUserStatus("offline");
        }
    }

    //    mempertivikasi apakah user terlah memiliki name
    public void VerifyUserExistance(){
//        mengambil id dari user
        String currentuserId = mAuth.getCurrentUser().getUid();
        RootRef.child("Users").child(currentuserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                jika sudah maka akan memberi toast
                if(dataSnapshot.child("name").exists()){
//                    jika belum maka akan ke halaman setting
                } else{
                    SendToSettingActivity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    // mengintent activity menuju loginactivity
    public void SendUserToLoginActivity(){
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    // call menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menus,menu);
        return true;
    }

    // define id option menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.main_logout_main_option){
//            make logout
            // update status to offline if done logout from acount
            updateUserStatus("offline");
            mAuth.signOut();
            SendUserToLoginActivity();
        }
        if(item.getItemId() == R.id.main_find_frind_option){
            SendToFindActivity();
        }
        if(item.getItemId() == R.id.main_create_group_option){
//          make group
            RequestNewGroupMain();
        }
        if(item.getItemId() == R.id.main_setting_main_option){

            SendToSettingActivity();
        }
        return super.onOptionsItemSelected(item);
    }

//    make group untuk pengguna
    private void RequestNewGroupMain() {
        AlertDialog.Builder builder = new  AlertDialog.Builder(MainActivity.this, R.style.AlertDialog);
        builder.setTitle("Enter your Group Name :");
        // make edittext
        final EditText groupNameField = new EditText(MainActivity.this);
        groupNameField.setHint("enter your group");
        builder.setView(groupNameField);
        // beri nama untuk button positive
        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String groupName = groupNameField.getText().toString();
                if (TextUtils.isEmpty(groupName)) {
                    Toast.makeText(MainActivity.this, "Please Enter your group ", Toast.LENGTH_SHORT).show();
                }
                else{
//                    jika memilih membuat group baru
                    CreateNewGroupMain(groupName);
                }
            }
        });
        // beri nama untuk button negatife
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
                builder.show();
    }

    // make grup
    // make new child the name is groups
    // make child is editText
    // edittext is enter name new groups
    // if make new jsonobject  gunakan child() if make value member of  jsongroup gunakan setValue("")
    private void CreateNewGroupMain(final String groupName ) {
        RootRef.child("Groups").child(groupName).setValue("")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(MainActivity.this, groupName+"Group baru is Create "  , Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void SendToSettingActivity(){
        Intent intent = new Intent(MainActivity.this, SettingActivity.class);
        startActivity(intent);
    }
    public void SendToFindActivity(){
        Intent intent = new Intent(MainActivity.this, FindFriendActivity.class);
        startActivity(intent);
    }
    // update time in pengguna
    void updateUserStatus (String state){
        String saveCurrentTime;
        String saveCurrentDate;
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentData = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentData.format(calendar.getTime());
        SimpleDateFormat currentData1= new SimpleDateFormat("hh:mm a");
        saveCurrentTime = currentData1.format(calendar.getTime());
        HashMap<String, Object> onlineState = new HashMap<>();
        onlineState.put("time", saveCurrentTime);
        onlineState.put("date", saveCurrentDate);
        onlineState.put("state", state);
        currentUserId = mAuth.getCurrentUser().getUid();
        RootRef.child("Users").child(currentUserId).child("userState").updateChildren(onlineState);
    }
}
