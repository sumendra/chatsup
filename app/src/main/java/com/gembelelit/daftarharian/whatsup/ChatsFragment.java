package com.gembelelit.daftarharian.whatsup;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFragment extends Fragment {

    View privateChat;
    RecyclerView recyclerView;
    DatabaseReference ChatRef;
    FirebaseAuth mAuth;
    String currentUserId;
    DatabaseReference UserRef;











    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        privateChat= inflater.inflate(R.layout.fragment_chats, container, false);
        mAuth = FirebaseAuth.getInstance();
        currentUserId=mAuth.getCurrentUser().getUid();
        ChatRef = FirebaseDatabase.getInstance().getReference().child("Contacts").child(currentUserId);
        UserRef=FirebaseDatabase.getInstance().getReference().child("Users");
        recyclerView=privateChat.findViewById(R.id.chatlist);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return privateChat;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<Context> option = new FirebaseRecyclerOptions.Builder<Context>()
                .setQuery(ChatRef, Context.class)
                .build();
        FirebaseRecyclerAdapter<Context, ChatViewHolder>adapter = new FirebaseRecyclerAdapter<Context, ChatViewHolder>(option) {
            @Override
            protected void onBindViewHolder(@NonNull final ChatViewHolder holder, int position, @NonNull Context model) {
                // menyimpan image di array karena kita mengirim image dari fragment menuju class agar gambar, tersimpan semua
                final String[] retImage = {"default_image"};
                final String Iduser = getRef(position).getKey();
                String userName;
                UserRef.child(Iduser).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()) {
                            if (dataSnapshot.hasChild("image")) {
                                retImage[0] = dataSnapshot.child("image").getValue().toString();
                                Picasso.get().load(retImage[0]).placeholder(R.drawable.image_profile).into(holder.profilImage);
                            }
                            final String retName = dataSnapshot.child("name").getValue().toString();
                            String retStatus = dataSnapshot.child("status").getValue().toString();
                            holder.userName.setText(retName);
                            holder.userStatus.setText(retStatus);

                            // jika userState memiliki anak state
                            if(dataSnapshot.child("userState").hasChild("state")){
                                String date = dataSnapshot.child("userState").child("date").getValue().toString();
                                String time = dataSnapshot.child("userState").child("time").getValue().toString();
                                String state = dataSnapshot.child("userState").child("state").getValue().toString();
                                if(state.equals("online")){
                                    holder.userStatus.setText("online");
                                }
                                // set status last online
                                else if(state.equals("offline")){
                                    holder.userStatus.setText("Last seen :"+ date+""+time);
                                }
                            }
                            // selain dari itu maka offline
                            else{
                                holder.userStatus.setText("offline");
                            }
                            holder.itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                                    intent.putExtra("visit_user_id", Iduser);
                                    intent.putExtra("visit_user_name", retName);
                                    intent.putExtra("visit_user_image", retImage[0]);
                                    startActivity(intent);
                                }
                            });
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @NonNull
            @Override
            public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_display_layout,viewGroup, false);
                return new ChatViewHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);
        adapter.startListening();;
    }
    public static class ChatViewHolder extends RecyclerView.ViewHolder {
        TextView userName, userStatus;
        CircleImageView profilImage;
        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);
            userName=itemView.findViewById(R.id.tvName);
            userStatus=itemView.findViewById(R.id.tvStatus);
            profilImage=itemView.findViewById(R.id.gambarpengguna);
        }
    }
}
