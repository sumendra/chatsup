package com.gembelelit.daftarharian.whatsup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingActivity extends AppCompatActivity {

    Button editButton;
    EditText etName, etStatus;
    CircleImageView userProfile;
    String currentUserId;
    FirebaseAuth mAuth;
    DatabaseReference RootRef;
    private static final int GallertPick=1;
    // menyimpan gambar di dalam storage
    private StorageReference UserProfileImageRef;
    private ProgressDialog loadingBar;
    Toolbar toolbar;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
//        initial firebase
        mAuth=FirebaseAuth.getInstance();
        currentUserId = mAuth.getCurrentUser().getUid();
        RootRef=FirebaseDatabase.getInstance().getReference();
        // call firebase profile with child Profile Images
        UserProfileImageRef = FirebaseStorage.getInstance().getReference().child("Profile Images");
//        initial id
        toolbar=findViewById(R.id.layoutsetting);
        setSupportActionBar(toolbar);
        editButton=findViewById(R.id.editbutton);
        etName=findViewById(R.id.edit_name);
        etStatus=findViewById(R.id.edit_status);
        userProfile=findViewById(R.id.profile_image);
        loadingBar = new ProgressDialog(this);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Setting");

//        default visibility edit name invisible
        etName.setVisibility(View.INVISIBLE);

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateSetting();
            }
        });

//        call select user name status untuk edit
        RetrieveUserInfo();
        // setOnclick listener
        userProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // intent to gallery
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent,GallertPick);
            }
        });
    }

    // crop image
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if result ok crop image
        if(requestCode==GallertPick && resultCode==RESULT_OK && data!=null){
            //crop image
            Uri imageUri = data.getData();
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }
        // if requestCode success
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if(resultCode == RESULT_OK){
                loadingBar.setTitle("Upload Process");
                loadingBar.setMessage("Please wait, upload image....");
                loadingBar.setCanceledOnTouchOutside(false);
                loadingBar.show();

                final Uri resultUri = result.getUri();
                // membuat id.jpg dimana id bersifat unik
                // jadi setiap melakukan edit image maka akan di beri nama tersebut
                final StorageReference filePats = UserProfileImageRef.child(currentUserId + ".jpg");
                // menyimpan resultUri dengan penamaan sesuai dengan path
                UploadTask uploadTask=filePats.putFile(resultUri);
                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return filePats.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            // mengambil hasil return berupa link download
                            Uri downloadUri = task.getResult();
                            // masukkan hasil dari download link ke dalam databse firebase image
                            RootRef.child("Users").child(currentUserId).child("image").setValue(downloadUri.toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(SettingActivity.this, "Image save in database success", Toast.LENGTH_SHORT).show();
                                        loadingBar.dismiss();
                                    } else {
                                        String message = task.getException().toString();
                                        Toast.makeText(SettingActivity.this, "Error" + message, Toast.LENGTH_SHORT).show();
                                        loadingBar.dismiss();
                                    }
                                }
                            });
                        }
                    }
                });

            }
        }
    }

    // edit name
    private void UpdateSetting() {
        String name = etName.getText().toString();
        String status = etStatus.getText().toString();
        if(TextUtils.isEmpty(name)){
            Toast.makeText(this, "required name", Toast.LENGTH_SHORT).show();
        }
        if(TextUtils.isEmpty(status)){
            Toast.makeText(this, "required status", Toast.LENGTH_SHORT).show();
        }
        else{
//            menyimpan nama status user dalam hashmap
            HashMap<String, Object> profileMap = new HashMap<>();
            profileMap.put("uid", currentUserId);
            profileMap.put("name", name);
            profileMap.put("status", status);
//            memasukkan penyimpanan kedalam Users berdasarkan UserId
            RootRef.child("Users").child(currentUserId).updateChildren(profileMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                SendUserToMainActivity();
                                Toast.makeText(SettingActivity.this, "Profile edit", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                String message = task.getException().toString();
                                Toast.makeText(SettingActivity.this, "Error"+message , Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }
    // send to intent mainactivity
    public void SendUserToMainActivity(){
        Intent in = new Intent(SettingActivity.this, MainActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(in);
        finish();
    }
//        select data when user update
    private void RetrieveUserInfo() {
        RootRef.child("Users").child(currentUserId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists() && dataSnapshot.hasChild("name") && dataSnapshot.hasChild("image")){
                            String retrieveUserName = dataSnapshot.child("name").getValue().toString();
                            String retrieveStatus = dataSnapshot.child("status").getValue().toString();
                            String retrieveProfileImage = dataSnapshot.child("image").getValue().toString();

                            etName.setText(retrieveUserName);
                            etStatus.setText(retrieveStatus);
                            // this link from databse firebase realtime
                            Picasso.get().load(retrieveProfileImage).into(userProfile);

                        }
                        else if(dataSnapshot.exists() && dataSnapshot.hasChild("name")){
                            String retrieveUserName = dataSnapshot.child("name").getValue().toString();
                            String retrieveStatus = dataSnapshot.child("status").getValue().toString();

                            etName.setText(retrieveUserName);
                            etStatus.setText(retrieveStatus);

                        }
                        else{
//                            ketika edit username belum terdapat maka akan divisibelity visible
                            etName.setVisibility(View.VISIBLE);
                            Toast.makeText(SettingActivity.this, "Please set & update your profile" , Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

}
// bedakan child dan value