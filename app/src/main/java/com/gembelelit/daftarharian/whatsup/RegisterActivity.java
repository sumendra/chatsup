package com.gembelelit.daftarharian.whatsup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class RegisterActivity extends AppCompatActivity {

    Button registerButton;
    EditText etEmailRegister, etPasswordRegister;
    TextView tvSudahMemilikiAkun;
    FirebaseAuth mAuth;
    ProgressDialog loadingBar;
    DatabaseReference Rootref;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // initialisasi id
        registerButton=findViewById(R.id.buatakun_registeractivity);
        etEmailRegister=findViewById(R.id.et_email_daftar_registeractivity);
        etPasswordRegister=findViewById(R.id.et_password_daftar_registeractivity);
        tvSudahMemilikiAkun=findViewById(R.id.tv_sudahmemiliki_akun_registeractivity);
//        call loadingbar
        loadingBar = new ProgressDialog(this);

        //call firevaseauth
        mAuth = FirebaseAuth.getInstance();
//        call database with firebase
        Rootref= FirebaseDatabase.getInstance().getReference();

        // make onclick
        tvSudahMemilikiAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendUserToLoginActivity();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateNewAccount();
            }
        });

    }

    private void CreateNewAccount() {
        String email = etEmailRegister.getText().toString();
        String password = etPasswordRegister.getText().toString();
        if(TextUtils.isEmpty(email)){
//            setelah registrasi maka akan langsung masuk ke mainactivity
            SendUserToMainActivity();
            Toast.makeText(this, "Please enter email ....", Toast.LENGTH_SHORT).show();
            loadingBar.dismiss();
        }
        if(TextUtils.isEmpty(password)){
            Toast.makeText(this, "Please enter password ....", Toast.LENGTH_SHORT).show();
            loadingBar.dismiss();
        }
        else{
//            make loadingbar whille proggress acccount
            loadingBar.setTitle("create New Account");
            loadingBar.setMessage("Please Wait, While we creating account ..");
            loadingBar.setCanceledOnTouchOutside(true);
            loadingBar.show();
            // this is create user call firebase auth
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                String devicetoken = FirebaseInstanceId.getInstance().getToken();
//                                mengambil value dari mAuth
                                String currentUserId = mAuth.getCurrentUser().getUid();
//                                menampung value token yang di ambil ke dalam table
                                Rootref.child("Users").child(currentUserId).setValue("");
                                Rootref.child("Users").child(currentUserId).child("device_token").setValue(devicetoken);
                                SendUserToMainActivity();
                                Toast.makeText(RegisterActivity.this, "Account Create succesful", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                String message = task.getException().toString();
                                Toast.makeText(RegisterActivity.this, "Error"+message, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    public void SendUserToLoginActivity(){
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }
    public void SendUserToMainActivity(){
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}

//catatan
//getUId merupakan token id dari user berdasarkan auth
// RootRef bagaikan root alu child artinya menambah child pada RootRef dalam bentuk json