package com.gembelelit.daftarharian.whatsup;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class RequestFragment extends Fragment {

    View requestView;
    RecyclerView recyclerView;
    private DatabaseReference ChatRequestRef, UserRef;
    DatabaseReference ContactRef;
    private FirebaseAuth mAuth;
    String currentUserId;











    public RequestFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        requestView= inflater.inflate(R.layout.fragment_request, container, false);
        mAuth = FirebaseAuth.getInstance();
        currentUserId = mAuth.getCurrentUser().getUid();
        UserRef=FirebaseDatabase.getInstance().getReference().child("Users");
        ContactRef = FirebaseDatabase.getInstance().getReference().child("Contacts");

        ChatRequestRef = FirebaseDatabase.getInstance().getReference().child("Chat Requests");
        recyclerView=requestView.findViewById(R.id.chat_request);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return requestView;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerOptions<Context> option = new FirebaseRecyclerOptions.Builder<Context>()
                .setQuery(ChatRequestRef,Context.class)
                .build();
        FirebaseRecyclerAdapter<Context,RequestViewHolder> adapter = new FirebaseRecyclerAdapter<Context, RequestViewHolder>(option) {
            @Override
            protected void onBindViewHolder(@NonNull final RequestViewHolder holder, int position, @NonNull Context model) {
                holder.itemView.findViewById(R.id.terima_button).setVisibility(View.VISIBLE);
                holder.itemView.findViewById(R.id.tolak_button).setVisibility(View.VISIBLE);
                // mengambil  didalam id dari setiap chat request
                final String Idlist = getRef(position).getKey();
                // take request type id chat request
                Toast.makeText(getActivity(), "user sekarang"+currentUserId, Toast.LENGTH_SHORT).show();
                final DatabaseReference getTypeRef = FirebaseDatabase.getInstance().getReference().child("Chat Requests").child(currentUserId).child(Idlist).child("request_type");
                getTypeRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        // take hasil request type send or receiver

                        if(dataSnapshot.exists()){
                            String type = dataSnapshot.getValue().toString();
                            if(type.equals("receiver")){
                                // ketika type equals receiver maka akan menampilkan nama status
                                UserRef.child(Idlist).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.hasChild("image")){
                                            String requestFileImage = dataSnapshot.child("image").getValue().toString();
                                            Picasso.get().load(requestFileImage).placeholder(R.drawable.image_profile).into(holder.profilImage);
                                        }
                                        final String requestUserName=dataSnapshot.child("name").getValue().toString();
                                        String requestUserStatus = dataSnapshot.child("status").getValue().toString();
                                        holder.userName.setText(requestUserName);
                                        holder.userStatus.setText(requestUserStatus);
//                                        give onclick menggunakan dialogInterface
                                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                CharSequence op[] = new CharSequence[]{
                                                        "Accept",
                                                        "Cancel"
                                                };
//                                                mmembuat dialog inteface pernyataan
                                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                builder.setTitle(requestUserName+"Chat Request");
                                                builder.setItems(op , new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
//                                                        ketika index equals 0
                                                        if(which == 0){
//                                                            tambah currentid lalu id pengirim lalu contact dengan value saved
                                                            ContactRef.child(currentUserId).child(Idlist).child("Contact").setValue("Saved").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if(task.isSuccessful()){
//                                                                        tambah id pengirim lalu current id lalu tambah Contact dengan value saved
                                                                        ContactRef.child(Idlist).child(currentUserId).child("Contact").setValue("Saved").addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                if(task.isSuccessful()){
//                                                                                    hapus request setelah menjadi Contact
                                                                                    ChatRequestRef.child(currentUserId).child(Idlist).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                        @Override
                                                                                        public void onComplete(@NonNull Task<Void> task) {
//                                                                                            hapus request setelah menjadi Contact
                                                                                            ChatRequestRef.child(Idlist).child(currentUserId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                                                @Override
                                                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                                                    if(task.isSuccessful()){
                                                                                                        Toast.makeText(getActivity(), "New Contact Added", Toast.LENGTH_SHORT).show();
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        if(which == 1){

                                                            ChatRequestRef.child(currentUserId).child(Idlist).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if(task.isSuccessful()){
                                                                        ChatRequestRef.child(Idlist).child(currentUserId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                if(task.isSuccessful()){
                                                                                    Toast.makeText(getActivity(), "cancel request" , Toast.LENGTH_SHORT).show();
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });

                                                        }
                                                    }
                                                });
                                                builder.show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                            // ketika merequest teman lalu ingin mencancel
                            else if(type.equals("send")){
                                Button request_send = holder.itemView.findViewById(R.id.terima_button);
                                request_send.setText("Request Send");
                                // menghilangkan button cancel, untuk membedakan yang mana di request oleh user itu sendiri lalu yang mana di request oleh user lain
                                holder.itemView.findViewById(R.id.tolak_button).setVisibility(View.INVISIBLE);
                                UserRef.child(Idlist).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.hasChild("image")){
                                            String requestFileImage = dataSnapshot.child("image").getValue().toString();
                                            Picasso.get().load(requestFileImage).placeholder(R.drawable.image_profile).into(holder.profilImage);
                                        }
                                        // membedakan antara request dari orang lain dan request dari dirinya sendiri
                                        final String requestUserName = dataSnapshot.child("name").getValue().toString();
                                        String requestUserStatus = dataSnapshot.child("status").getValue().toString();
                                        holder.userName.setText(requestUserName);
                                        holder.userStatus.setText("request to " + requestUserName );
                                        // cancel request
                                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                CharSequence oprl[] = new CharSequence[]{
                                                        "Cancel Chat Request"
                                                };
                                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                builder.setTitle("Already Send Request");
                                                builder.setItems(oprl, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // ketika memilih index null
                                                        if (which == 0) {
                                                            ChatRequestRef.child(currentUserId).child(Idlist).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {
                                                                        ChatRequestRef.child(Idlist).child(currentUserId).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                Toast.makeText(getActivity(), "you have canceled chat request", Toast.LENGTH_SHORT).show();
                                                                            }
                                                                        });
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }

                                                });
                                                builder.show();
                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @NonNull
            @Override
            public RequestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_display_layout, viewGroup, false );
                RequestViewHolder holder = new RequestViewHolder(view);
                return holder;
            }
        };
        recyclerView.setAdapter(adapter);
        adapter.startListening();
    }
    public static class RequestViewHolder extends RecyclerView.ViewHolder {
        TextView userName, userStatus;
        CircleImageView profilImage;
        Button acceptButton, cancelButton;
        public RequestViewHolder(@NonNull View itemView) {
            super(itemView);
            userName=itemView.findViewById(R.id.tvName);
            userStatus=itemView.findViewById(R.id.tvStatus);
            profilImage=itemView.findViewById(R.id.gambarpengguna);
            acceptButton=itemView.findViewById(R.id.terima_button);
            cancelButton=itemView.findViewById(R.id.tolak_button);
        }
    }
}
