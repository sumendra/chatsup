package com.gembelelit.daftarharian.whatsup;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class PhoneLoginActivity extends AppCompatActivity {

    private Button SendVertifikasi ;
    private Button Vertifikasi;
    private EditText etPhoneNumber;
    private EditText etNumberVertify;


    // send token vertifikasi telepon
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    // check token vertifikasi user
    private String mVertivicationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private FirebaseAuth mAu;
    // make loading
    private ProgressDialog loadingBar;









    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_login);
//        inilisasi id
        etPhoneNumber=findViewById(R.id.etNumberPhone);
        etNumberVertify=findViewById(R.id.etVertificationNumber);
        Vertifikasi=findViewById(R.id.button2);
        SendVertifikasi=findViewById(R.id.button_send);
        loadingBar = new ProgressDialog(this);
        mAu=FirebaseAuth.getInstance();

        // send vertificatoion or onclick sendVertivication
        SendVertifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // pluss loading bar
                loadingBar.setTitle("Phone vertivication");
                loadingBar.setMessage("please wait, while are notification...");
                loadingBar.setCanceledOnTouchOutside(false);
                loadingBar.show();
                String phoneNumber = etPhoneNumber.getText().toString().trim();
                if(TextUtils.isEmpty(phoneNumber)){
                    Toast.makeText(PhoneLoginActivity.this,  "required number" , Toast.LENGTH_SHORT).show();
                }
                else{
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            phoneNumber,        // Phone number to verify
                            60,                 // Timeout duration
                            TimeUnit.SECONDS,   // Unit of timeout
                            PhoneLoginActivity.this,               // Activity (for callback binding)
                            callbacks);        // OnVerificationStateChangedCallbacks
                }
            }
        });

        // melakukan vertifikasi setelah code dikirimm
        Vertifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendVertifikasi.setVisibility(View.INVISIBLE);
                etPhoneNumber.setVisibility(View.INVISIBLE);
                String vertificationCode = etNumberVertify.getText().toString().trim();
                if(vertificationCode.isEmpty()){
                    Toast.makeText(PhoneLoginActivity.this, "required vertification" , Toast.LENGTH_SHORT).show();
                }
                else{
                    // pluss loading bar
                    loadingBar.setTitle("Vertification Code");
                    loadingBar.setMessage("please wait, while vertivication code....");
                    loadingBar.setCanceledOnTouchOutside(false);
                    loadingBar.show();
                    // mengecek apkah code vertifikasi benar
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVertivicationId, vertificationCode);
//                    jika benar maka akan langsung sigin ke main activity
                    signInWithPhoneAuthCredential(credential);
                }
            }
        });

//      merequest  send vertifikasi from server to device
        callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
//                ketika vertifikasi komplete
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                loadingBar.dismiss();
                Toast.makeText(PhoneLoginActivity.this, "Invalit vertifikasi Phone number, please enter valid vertifikasi", Toast.LENGTH_SHORT).show();
                SendVertifikasi.setVisibility(View.VISIBLE);
                etPhoneNumber.setVisibility(View.VISIBLE);

                Vertifikasi.setVisibility(View.INVISIBLE);
                etNumberVertify.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token)  {
                // Save verification ID and resending token so we can use them later
                mVertivicationId = verificationId;
//                ketika vertifikasi berhasil di send
                mResendToken = token;
                // dismis loading bar ketika token sudah di send ke device user
                loadingBar.dismiss();
                Toast.makeText(PhoneLoginActivity.this, "Code hash been send " , Toast.LENGTH_SHORT).show();
                // if send vertivikasi clik then sendvertifikasi visibiliti gone
                SendVertifikasi.setVisibility(View.INVISIBLE);
                etPhoneNumber.setVisibility(View.INVISIBLE);

                // if vertifiksi klik then to mainacktivity
                Vertifikasi.setVisibility(View.VISIBLE);
                etNumberVertify.setVisibility(View.VISIBLE);

            }
        };
    }
    // if vertivication success can sigin menggunakan no telp
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAu.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // if success sign in loading bar can dismiss
                            loadingBar.dismiss();
                            Toast.makeText(PhoneLoginActivity.this, "Success sign in with phone number" , Toast.LENGTH_SHORT).show();
                            SendUserToMainActivity();
                        } else {
                            loadingBar.dismiss();
                            String message = task.getException().toString();
                            Toast.makeText(PhoneLoginActivity.this, "gagal vertification"+message , Toast.LENGTH_SHORT).show();

                            }
                        }

                });
    }
    // send to main activity
    public void SendUserToMainActivity(){
        Intent intent = new Intent(PhoneLoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
