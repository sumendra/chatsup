package com.gembelelit.daftarharian.whatsup;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Toolbar toolbar;
    ImageButton sendButton;
    EditText etChat;
    String userName;
    String userId;
    TextView userNameHeader;
    TextView userOnlineHeader;
    CircleImageView imageProfile;
    String userImage;
    FirebaseAuth mAuth;
    String messageSenderId;
    DatabaseReference RootRef;
    List<Messages> messageList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    MessageAdapter adapter;
    ImageButton sendFile;
    String saveCurrentTime, saveCurrentDate;
    private String cheker = "";
    private Uri fileUri;
    private String myUri=" ";
    // to take data in firebase storage
    private StorageTask uploadTask;
    private ProgressDialog loadingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        // initial id
        sendFile=findViewById(R.id.sendFile);
        recyclerView=findViewById(R.id.chatrecycler);
        toolbar=findViewById(R.id.chat_toolbar);
        sendButton=findViewById(R.id.sendButton);
        etChat=findViewById(R.id.etChat);
        // call intent
        userName=getIntent().getExtras().get("visit_user_name").toString();
        userId=getIntent().getExtras().get("visit_user_id").toString();
        userImage=getIntent().getExtras().get("visit_user_image").toString();
        // initial firebase
        RootRef= FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        messageSenderId = mAuth.getCurrentUser().getUid();
        // inialisasi loading bar
        loadingBar = new ProgressDialog(this);
        // initial toolbar
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        // take custome bar view to user display layout
        LayoutInflater layoutInflater= (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = layoutInflater.inflate(R.layout.custom_chat_bar, null);
        actionBar.setCustomView(actionBarView, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        // initial id custome bar
        userNameHeader=findViewById(R.id.tvNameChat);
        userOnlineHeader=findViewById(R.id.tvOnlineChat);
        imageProfile=findViewById(R.id.custom_profil_image);
        //settext name image
        userNameHeader.setText(userName);
        Picasso.get().load(userImage).placeholder(R.drawable.image_profile).into(imageProfile);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // send message
                SendMessage();
            }
        });
        // inialisati adapter
        adapter = new MessageAdapter(messageList);
        linearLayoutManager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        // display last online
        DisplayLastOnline();
        // initial calendar
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());
        SimpleDateFormat currentTime = new SimpleDateFormat("hh:mm a");
        saveCurrentTime = currentTime.format(calendar.getTime());
        // send file
        sendFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence op[] = new CharSequence[]{
                        "Images",
                        "PDF Files",
                        "Mr Word Files"
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                builder.setTitle("Select the file");
                builder.setItems(op, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            // status yang di dikirim
                            cheker = "image";
                            // intent to galery
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("image/*");
                            // make choose file
                            startActivityForResult(Intent.createChooser(intent,"select Image"), 438);

                        }
                        if(which == 1){
                            cheker = "pdf";
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/pdf");
                            startActivityForResult(Intent.createChooser(intent,"Select PDF File "),438);
                        }
                        if(which == 2){
                            cheker = "docx";
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/msword");
                            startActivityForResult(Intent.createChooser(intent,"Select Ms Word File "),438);
                        }
                    }
                });
                builder.show();
            }
        });
    }

    // to take image from gallery
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 438 && resultCode==RESULT_OK && data!=null && data.getData()!=null){
            loadingBar.setTitle("Sending Process");
            loadingBar.setMessage("Please wait, Sending image....");
            loadingBar.setCanceledOnTouchOutside(false);
            loadingBar.show();
            // select link for image
            fileUri = data.getData();
            // when checker dont image
            if(!cheker.equals("image")){
                // to take data in firebase storage
                // image files merupakan penamaan yang akan tampil pada fireabse storage
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(" DocumentFile");
                // send message to firebase database to location
                final String messageSenderRef= "message/"+messageSenderId+"/"+userId;
                final String messageReceiverRef = "message/"+userId+"/"+messageSenderId;
                DatabaseReference userMessageKey = RootRef.child("Messages").child(messageSenderId).child(userId).push();
                // mengambil key dari message yaitu key dari message setelah mengakses current id dan receiver id maka akan terdapat key baru yang di gunakan untuk membedakanr atnara satu message dengan message yang lain
                final String messagePushId = userMessageKey.getKey();
                // to save data in firebase storage
                // give the name file is message id push.jpg
                final StorageReference filePath = storageReference.child(messagePushId + "."+cheker);
                uploadTask = filePath.putFile(fileUri);
                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw  task.getException();
                        }
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            // mengambil hasil return berupa link download
                            Uri downloadUri = task.getResult();
                            myUri = downloadUri.toString();
                            Map messageTextBody = new HashMap();
                            messageTextBody.put("message", myUri);
                            // mengambil filePath terakhir dalam StorageReference filePaths
                            messageTextBody.put("name", fileUri.getLastPathSegment());
                            messageTextBody.put("type", cheker);
                            messageTextBody.put("from", messageSenderId);
                            messageTextBody.put("to",userId);
                            // id dari message
                            messageTextBody.put("messageId", messagePushId);
                            messageTextBody.put("time", saveCurrentTime);
                            messageTextBody.put("date", saveCurrentDate);
                            // menampung di hasmap agar message sender disimpan  bersanaan dengan message receiver dengan child message push id messagetextbody
                            Map messageBodyDetail = new HashMap();
                            // message push id pembuat uniq file
                            messageBodyDetail.put(messageSenderRef+"/"+messagePushId, messageTextBody);
                            messageBodyDetail.put(messageReceiverRef+"/"+messagePushId, messageTextBody);
                            RootRef.updateChildren(messageBodyDetail);
                            loadingBar.dismiss();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        loadingBar.dismiss();
                        Toast.makeText(ChatActivity.this, "error"+e.getMessage() , Toast.LENGTH_SHORT).show();
                    }
                });
                uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {

                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // make loading progreess
                        double p = (100.0*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
                        loadingBar.setMessage((int) p+ "% Uploading....");
                        loadingBar.setTitle("updload data");
                        loadingBar.show();
                    }
                });
                Intent intent = new Intent(ChatActivity.this, MainActivity.class);
                startActivity(intent);
            }
            else if(cheker.equals("image")){
                // to take data in firebase storage
                // image files merupakan penamaan yang akan tampil pada fireabse storage
                StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("Image File");
                // send message to firebase database to location
                final String messageSenderRef= "message/"+messageSenderId+"/"+userId;
                final String messageReceiverRef = "message/"+userId+"/"+messageSenderId;
                DatabaseReference userMessageKey = RootRef.child("Messages").child(messageSenderId).child(userId).push();
                // mengambil key dari message yaitu key dari message setelah mengakses current id dan receiver id maka akan terdapat key baru yang di gunakan untuk membedakanr atnara satu message dengan message yang lain
                final String messagePushId = userMessageKey.getKey();
                // to save data in firebase storage
                // give the name file is message id push.jpg
                final StorageReference filePath = storageReference.child(messagePushId + "."+"jpg");
                uploadTask = filePath.putFile(fileUri);
                // upload file
                uploadTask.continueWithTask(new Continuation() {
                    @Override
                    public Object then(@NonNull Task task) throws Exception {
                        if(!task.isSuccessful()){
                            throw  task.getException();
                        }
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            // mengambil hasil return berupa link download
                            Uri downloadUri = task.getResult();
                            myUri = downloadUri.toString();
                            Map messageTextBody = new HashMap();
                            messageTextBody.put("message", myUri);
                            // mengambil filePath terakhir dalam StorageReference filePaths
                            messageTextBody.put("name", fileUri.getLastPathSegment());
                            messageTextBody.put("type", cheker);
                            messageTextBody.put("from", messageSenderId);
                            messageTextBody.put("to",userId);
                            // id dari message
                            messageTextBody.put("messageId", messagePushId);
                            messageTextBody.put("time", saveCurrentTime);
                            messageTextBody.put("date", saveCurrentDate);
                            // menampung di hasmap agar message sender disimpan  bersanaan dengan message receiver dengan child message push id messagetextbody
                            Map messageBodyDetail = new HashMap();
                            // message push id pembuat uniq file
                            messageBodyDetail.put(messageSenderRef+"/"+messagePushId, messageTextBody);
                            messageBodyDetail.put(messageReceiverRef+"/"+messagePushId, messageTextBody);
                            RootRef.updateChildren(messageBodyDetail).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    if(!task.isSuccessful()){
                                        loadingBar.dismiss();
                                        String messgae = task.getException().getMessage().toString();
                                        Toast.makeText(ChatActivity.this, "errror"+messgae , Toast.LENGTH_SHORT).show();
                                    }
                                    else{
                                        Toast.makeText(ChatActivity.this, "success send message", Toast.LENGTH_SHORT).show();
                                        loadingBar.dismiss();
                                        Intent intent = new Intent(ChatActivity.this, MainActivity.class);
                                        startActivity(intent);
                                    }
                                    etChat.setText("");
                                }
                            });
                        }
                    }
                });

            }
            else{
                Toast.makeText(this, "Nothing  Selected, Error", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Intent intent = new Intent(ChatActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    public void DisplayLastOnline(){
        RootRef.child("Users").child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // ketika userState memiliki anak state
                if(dataSnapshot.child("userState").hasChild("state")){
                    String state = dataSnapshot.child("userState").child("state").getValue().toString();
                    String date = dataSnapshot.child("userState").child("date").getValue().toString();
                    String time = dataSnapshot.child("userState").child("time").getValue().toString();
                    //ketika value dari state online
                    if(state.equals("online")){
                        userOnlineHeader.setText("online");
                    }
                    // ketika value dari state offline
                    else if(state.equals("offline")){
                        userOnlineHeader.setText(date+" "+time);
                    }
                }
                // ketika userState tidak memiliki anak
                else{
                    userOnlineHeader.setText("offline");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void SendMessage(){
        // take edittext
        String messageText = etChat.getText().toString().trim();
        if(TextUtils.isEmpty(messageText)){
            Toast.makeText(this, "required message" , Toast.LENGTH_SHORT).show();
        }
        else{
            // send message to firebase database to location
            String messageSenderRef= "message/"+messageSenderId+"/"+userId;
            String messageReceiverRef = "message/"+userId+"/"+messageSenderId;
            DatabaseReference userMessageKey = RootRef.child("Messages").child(messageSenderId).child(userId).push();
            // mengambil key dari message yaitu key dari message setelah mengakses current id dan receiver id maka akan terdapat key baru yang di gunakan untuk membedakanr atnara satu message dengan message yang lain
            String messagePushId = userMessageKey.getKey();
            Map messageTextBody = new HashMap();
            messageTextBody.put("message", messageText);
            messageTextBody.put("type", "text");
            messageTextBody.put("from", messageSenderId);
            messageTextBody.put("to",userId);
            messageTextBody.put("messageId", messagePushId);
            messageTextBody.put("time", saveCurrentTime);
            messageTextBody.put("date", saveCurrentDate);
            // menampung di hasmap agar message sender disimpan  bersanaan dengan message receiver dengan child message push id messagetextbody
            Map messageBodyDetail = new HashMap();
            // kenapa tidak update melainkan insert
            // karena data yang di tuju tidak ada
            // setiap messagePush id memiliki nilai yang uniq
            // sehingga setiap kali mengupdate  message  akan membuat message baru
            messageBodyDetail.put(messageSenderRef+"/"+messagePushId, messageTextBody);
            messageBodyDetail.put(messageReceiverRef+"/"+messagePushId, messageTextBody);
            RootRef.updateChildren(messageBodyDetail).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(!task.isSuccessful()){
                        String messgae = task.getException().getMessage().toString();
                        Toast.makeText(ChatActivity.this, "errror"+messgae , Toast.LENGTH_SHORT).show();
                    }
                    etChat.setText("");
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        RootRef.child("message").child(messageSenderId).child(userId).addChildEventListener(new ChildEventListener() {
            // id dari user yang dikirim  akan di kirim ke adapter melalui array list
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if(dataSnapshot.exists()) {
                    // mengambil semua value darit databse lalu di tampng ke message class
                    Messages messages = dataSnapshot.getValue(Messages.class);
                    // mamasukkan messages ke dalam arraylist
                    messageList.add(messages);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
